/*
 * Copyright By @2dgirlismywaifu (2023) .
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.notelysia.restservices.controller;

import com.notelysia.restservices.exception.ResourceNotFound;
import com.notelysia.restservices.model.entity.NewsSource;
import com.notelysia.restservices.service.NewsSourceServices;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/news-app")
@Tag(name = "News Source", description = "API for News Source")
public class NewsSourceController {
    private Map<String, NewsSource> respond;
    @Autowired
    private NewsSourceServices newsSourceServices;

    @GetMapping(value = "/guest/news-source")
    public ResponseEntity<Map<String, List<NewsSource>>> allNewsSource() {
        Map<String, List<NewsSource>> listrespond = new HashMap<>();
        listrespond.put("newsSource", this.newsSourceServices.findAllNewsSource());
        return new ResponseEntity<>(listrespond, HttpStatus.OK);
    }

    //For user login
    @GetMapping(value = "/account/news-source", params = {"userid"})
    public ResponseEntity<Map<String, NewsSource>> userNewsSource(
            @RequestParam(value = "userid") int userid
    ) throws ResourceNotFound {
        this.respond = new HashMap<>();
        NewsSource newsSources = this.newsSourceServices.findByUserId(userid)
                .orElseThrow(() -> new ResourceNotFound("Failed"));
        this.respond.put("newsSource", newsSources);
        return new ResponseEntity<>(this.respond, HttpStatus.OK);
    }

}
