/*
 * Copyright By @2dgirlismywaifu (2024) .
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.notelysia.restservices.service.impl;

import com.notelysia.restservices.model.dto.AuthApiKeyDto;
import com.notelysia.restservices.model.entity.AuthApiKey;
import com.notelysia.restservices.repository.AuthApiKeyRepo;
import com.notelysia.restservices.service.AuthApiKeyServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class AuthApiKeyServicesImpl implements AuthApiKeyServices {
    @Autowired
    private AuthApiKeyRepo authApiKeyRepo;
    @Override
    public Optional<AuthApiKeyRepo> findByHeader(String headerName, String token) {
        return this.authApiKeyRepo.findByHeader(headerName, token);
    }

    @Override
    public void saveAuthApiKey(AuthApiKeyDto authApiKeyDto) {
        AuthApiKey authApiKey = new AuthApiKey();
        authApiKey.setId(authApiKeyDto.getId());
        authApiKey.setHeaderName(authApiKeyDto.getHeaderName());
        authApiKey.setToken(authApiKeyDto.getToken());
        authApiKey.setIsEnable(1);
        this.authApiKeyRepo.save(authApiKey);
    }

    @Override
    public void disableKey(String headerName, String token) {
        this.authApiKeyRepo.disableKey(headerName, token);
    }
}
