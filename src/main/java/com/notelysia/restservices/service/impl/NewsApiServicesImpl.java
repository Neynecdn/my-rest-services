package com.notelysia.restservices.service.impl;

import com.notelysia.restservices.model.entity.NewsAPICountry;
import com.notelysia.restservices.repository.NewsApiRepo;
import com.notelysia.restservices.service.NewsApiServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NewsApiServicesImpl implements NewsApiServices {
    @Autowired
    private NewsApiRepo newsApiRepo;

    @Override
    public List<NewsAPICountry> findAll() {
        return this.newsApiRepo.findAll();
    }

    @Override
    public String findByCountryName(String country) {
        return this.newsApiRepo.findByCountry(country);
    }
}
