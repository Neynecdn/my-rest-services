package com.notelysia.restservices.service;

import com.notelysia.restservices.model.entity.SyncNewsFavourite;
import com.notelysia.restservices.model.entity.SyncSubscribe;

import java.util.List;

public interface SyncServices {
    void saveNewsFavourite(SyncNewsFavourite syncNewsFavourite);

    void deleteNewsFavourite(String userId, String url, String title, String imageUrl, String sourceName);

    List<SyncNewsFavourite> findByUserId(int userId);

    SyncNewsFavourite findSyncNewsFavouriteBy(int userId, String sourceId, String title, String imageUrl, String sourceName);

    void saveSubscribe(SyncSubscribe syncSubscribe);

    void deleteByUserIdAndSourceId(int userId, String sourceId);

    SyncSubscribe findByUserIdAndSourceId(int userId, String sourceId);

}
