package com.notelysia.restservices.service;

import com.notelysia.restservices.model.dto.RSSList;
import com.notelysia.restservices.model.entity.NewsDetail;
import com.notelysia.restservices.model.entity.NewsSource;

import java.util.List;
import java.util.Optional;

public interface NewsSourceServices {
    List<NewsSource> findAllNewsSource();

    Optional<NewsSource> findByUserId(int useId);

    List<NewsDetail> findByUrlTypeAndSourceName(String urlType, String sourceName);

    List<NewsDetail> findBySourceName(String sourceName);

    List<RSSList> findUrlBySourceName(String sourceName);
}
