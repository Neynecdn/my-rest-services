package com.notelysia.restservices.service;

import com.notelysia.restservices.model.entity.NewsAPICountry;

import java.util.List;

public interface NewsApiServices {
    List<NewsAPICountry> findAll();

    String findByCountryName(String country);
}
